# PiFace

PiFace Project

## Components

- Raspberry Pi Zero W

    https://www.raspberrypi.org/products/raspberry-pi-zero-w/

    https://www.amazon.com/CanaKit-Raspberry-inal%C3%A1mbrico-oficial-alimentaci%C3%B3n/dp/B071L2ZQZX

- Waveshare 3.5" touchscreen

    https://www.waveshare.com/product/raspberry-pi/displays/lcd-oled/3.5inch-rpi-lcd-a.htm
    
    https://www.amazon.com/Waveshare-Raspberry-Resistive-Interface-Rapsberry-pi/dp/B00OZLG2YS

## Installation

- Flash the Raspbian Stretch **Lite** to your micro SD Card and plug it in.

    http://downloads.raspberrypi.org/raspbian_lite/images/raspbian_lite-2019-04-09/2019-04-08-raspbian-stretch-lite.zip

- Configure the WiFi of your RPi Zero W to get connected to the Internet.

- Clone this repository

    ```shell script
      sudo apt update
      sudo apt install -y git
      cd ~
      git clone https://gitlab.com/rpiguru/PiFace  
    ```
  
- Install everything

    ```shell script
      cd PiFace
      bash setup.sh
    ```

- And reboot!
