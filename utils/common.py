import gc
import json
import os
import subprocess
import sys
import logging.config
import tempfile

import netifaces
import platform

_cur_dir = os.path.dirname(os.path.realpath(__file__))
_par_dir = os.path.join(_cur_dir, os.path.pardir)
if _par_dir not in sys.path:
    sys.path.append(_par_dir)


logging.config.fileConfig(os.path.join(_cur_dir, 'logging.ini'))
logger = logging.getLogger('PF')


try:
    is_rpi = 'arm' in os.uname()[4]
except AttributeError:
    is_rpi = False


def update_dict_recursively(dest, updated):
    """
    Update dictionary recursively.
    :param dest: Destination dict.
    :type dest: dict
    :param updated: Updated dict to be applied.
    :type updated: dict
    :return:
    """
    for k, v in updated.items():
        if isinstance(dest, dict):
            if isinstance(v, dict):
                r = update_dict_recursively(dest.get(k, {}), v)
                dest[k] = r
            else:
                dest[k] = updated[k]
        else:
            dest = {k: updated[k]}
    return dest


def check_running_proc(proc_name):
    """
    Check if a process is running or not
    :param proc_name:
    :return:
    """
    is_running = False
    cmd = "ps -aef | grep -i '%s' | grep -v 'grep' | awk '{ print $3 }'" % proc_name
    try:
        if len(os.popen(cmd).read().strip().splitlines()) > 0:
            is_running = True
    except Exception as e:
        print(f'Failed to get status of the process({proc_name}) - {e}')
    return is_running


def get_screen_resolution():
    """
    Get resolution of the screen
    :return:
    """
    if is_rpi:
        pipe = os.popen('fbset -s')
        data = pipe.read().strip()
        pipe.close()
        for line in data.splitlines():
            if line.startswith('mode'):
                w, h = [int(p) for p in line.split('"')[1].split('x')]
                return w, h
    else:
        return 480, 320


def get_ip_address(ifname='wlan0'):
    """
    Get assigned IP address of given interface
    :param ifname: interface name such as wlan0, eth0, etc
    :return: If not on RPi, returns the IP address of LAN
    """
    if platform.system() == 'Windows':
        return '192.168.1.170'
    try:
        return netifaces.ifaddresses(ifname)[netifaces.AF_INET][0]['addr']
    except Exception as e:
        print(f'Failed to get IP address of {ifname}, reason: {e}')


def get_current_ap():
    """
    Get SSID of the current AP
    :return:
    """
    pipe = os.popen('/sbin/iwgetid -r')
    ap = pipe.read().strip()
    pipe.close()
    return ap


def get_mac_address(interface='wlan0'):
    try:
        mac_str = open('/sys/class/net/' + interface + '/address').read()
    except Exception as e:
        logger.error(f'Failed to get MAC address of {interface}: {e}')
        mac_str = "00:00:00:00:00:00"
    return mac_str[0:17]


def number_to_ordinal(n):
    """
    Convert number to ordinal number string
    """
    return "%d%s" % (n, "tsnrhtdd"[(n / 10 % 10 != 1) * (n % 10 < 4) * n % 10::4])


def get_free_gpu_size():
    gc.collect()
    if is_rpi:
        try:
            pipe = os.popen('sudo vcdbg reloc stats | grep "free memory"')
            data = pipe.read().strip()
            pipe.close()
            return data
        except OSError:
            logger.error('!!! Failed to get free GPU size! ')
            tot, used, free = map(int, os.popen('free -t -m').readlines()[-1].split()[1:])
            logger.error('Total: {}, Used: {}, Free: {}'.format(tot, used, free))
    return 0


def disable_screen_saver():
    if is_rpi:
        os.system('sudo sh -c "TERM=linux setterm -blank 0 >/dev/tty0"')


if is_rpi:
    _json_file = "/home/pi/pi_face_info.json"
else:
    _json_file = os.path.join(tempfile.gettempdir(), 'pi_face_info.json')


def update_dict_recursively(dest, updated):
    """
    Update dictionary recursively.
    :param dest: Destination dict.
    :type dest: dict
    :param updated: Updated dict to be applied.
    :type updated: dict
    :return:
    """
    for k, v in updated.items():
        if isinstance(dest, dict):
            if isinstance(v, dict):
                r = update_dict_recursively(dest.get(k, {}), v)
                dest[k] = r
            else:
                dest[k] = updated[k]
        else:
            dest = {k: updated[k]}
    return dest


def get_config():
    if os.path.exists(_json_file):
        return json.loads(open(_json_file).read())
    else:
        return {}


def update_config_file(data):
    old_data = update_dict_recursively(dest=get_config(), updated=data)
    with open(_json_file, 'w') as jp:
        json.dump(old_data, jp, indent=2)


def save_info(data):
    with open(_json_file, 'w') as jp:
        json.dump(data, jp, indent=2)


def get_serial():
    """
    Get serial number of the device
    :return:
    """
    if is_rpi:
        cpuserial = "0000000000000000"
        f = open('/proc/cpuinfo', 'r')
        for line in f:
            if line[0:6] == 'Serial':
                cpuserial = line[10:26].lstrip('0')
        f.close()
        return cpuserial
    else:
        return '12345678'


def scan_wifi_networks():
    if is_rpi:
        iwlist_raw = subprocess.Popen(['/sbin/iwlist', 'wlan0', 'scan'], stdout=subprocess.PIPE)
        ap_list, err = iwlist_raw.communicate()
        ap_array = []
        for line in ap_list.decode('utf-8').rsplit('\n'):
            if 'ESSID' in line:
                ap_ssid = line[27:-1]
                if ap_ssid != '':
                    ap_array.append(ap_ssid)
    else:
        ap_array = [f'AP {i}' for i in range(10)]
    return ap_array


def create_wifi_config(ssid, password):
    config_lines = [
        '\n',
        'network={',
        '\tssid="{}"'.format(ssid),
        '\tpsk="{}"'.format(password),
        '\tkey_mgmt=WPA-PSK',
        '}'
    ]
    config = '\n'.join(config_lines)

    if is_rpi:
        with open("/etc/wpa_supplicant/wpa_supplicant.conf", "a+") as wifi:
            wifi.write(config)
    else:
        print(config)


def set_room_type(room_type, description):
    update_config_file({'room_type': room_type, 'room_description': description})
