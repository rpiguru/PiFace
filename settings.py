import os

BASE_DIR = os.path.expanduser('~/.PF')
os.makedirs(BASE_DIR, exist_ok=True)

DEBUG = False

SERVER_HOST = '0.0.0.0'
SERVER_PORT = 80

try:
    from local_settings import *
except ImportError:
    pass
